function reverse(params) {
  if (typeof params !== "string") throw Error("params should be a string");
  return params.split("").reverse().join("");
}

export { reverse };
